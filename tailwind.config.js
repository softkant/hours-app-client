module.exports = {
  purge: {
    enabled: false,
    content: [ './src/**/*.{html,ts}']
  },
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {opacity: ['disabled']},
  },
  plugins: [],
}