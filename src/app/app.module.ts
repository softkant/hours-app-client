import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { CoreComponent } from './core/core.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from './core/core.module';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { CookieModule } from 'ngx-cookie';
import { NgApexchartsModule } from "ng-apexcharts";

@NgModule({
  declarations: [],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    CoreModule,
    GraphQLModule,
    HttpClientModule,
    NgApexchartsModule,
    CookieModule.forRoot()
  ],
  providers: [],
  bootstrap: [CoreComponent]
})
export class AppModule {

 }
