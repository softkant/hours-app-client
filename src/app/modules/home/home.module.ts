import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { IndexComponent } from './pages/index/index.component';
import { HeaderComponent } from './layout/header/header.component';
import { LayoutHomeComponent } from './layout/layout-home.component';
import { CookieModule } from 'ngx-cookie';
import { SharedModule } from '@shared/shared.module';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';

@NgModule({
  declarations: [LayoutHomeComponent,IndexComponent, HeaderComponent, SignInComponent, SignUpComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CookieModule.forChild(),
    SharedModule
  ],
  providers:[]
})
export class HomeModule { }
