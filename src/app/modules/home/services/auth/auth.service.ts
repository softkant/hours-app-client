import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { REGISTER_USER } from '@modules/home/graphql/auth/mutations';
import { LOGIN_QUERY } from '@modules/home/graphql/auth/queries';
import { IAuth, IUser, TAuthLogin, TAuthRegister } from '@modules/home/models/User';
import { Apollo } from 'apollo-angular';
import { CookieService } from 'ngx-cookie';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  cookie: string = "auth";

  constructor(private apollo: Apollo, private cookieService: CookieService, private router: Router) {
  }


  login(user: IAuth) {
    return this.apollo.query<TAuthLogin>({
      query: LOGIN_QUERY,
      fetchPolicy: 'network-only',
      variables: {
        user: user
      }
    });
  }

  register(user: IUser) {
   return this.apollo.mutate<TAuthRegister>({
      mutation: REGISTER_USER,
      variables: {
        user: user
      }
    })
  }

  logout(): void {
    this.apollo.client.resetStore();
    this.cookieService.remove(this.cookie);
    this.router.navigate(["/"]);
  }

  isAuthenticated(): boolean {
    const hasKey = this.cookieService.hasKey(this.cookie);
    if (!hasKey)
      this.removeUserId();

    return hasKey;
  }

  getUserId(): string | null {
    return localStorage.getItem("local_key");
  }

  setUserId(id: any) {
    localStorage.setItem("local_key", id);
  }

  private removeUserId() {
    localStorage.removeItem("local_key");
  }

  navigateToProjects() {
    this.router.navigate(["/projects"])
  }

}
