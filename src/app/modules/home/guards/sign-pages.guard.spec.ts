import { TestBed } from '@angular/core/testing';

import { SignPagesGuard } from './sign-pages.guard';

describe('SignPagesGuard', () => {
  let guard: SignPagesGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(SignPagesGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
