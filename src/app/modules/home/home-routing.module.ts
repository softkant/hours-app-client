import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignPagesGuard } from './guards/sign-pages.guard';
import { IndexComponent } from './pages/index/index.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: 'login',
    component: SignInComponent,
    canActivate: [SignPagesGuard]
  },
  {
    path: 'register',
    component: SignUpComponent,
    canActivate: [SignPagesGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
