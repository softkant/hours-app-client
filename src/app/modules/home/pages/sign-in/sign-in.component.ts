import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@modules/home/services/auth/auth.service';

@Component({
  selector: 'hours-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  loginForm!: FormGroup;
  isLoading: boolean = true;
  error!: string;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  login(): void {
    this.authService.login(this.loginForm.getRawValue()).subscribe(({ data, loading }) => {
      if (data.login.error !== null) {
        this.error = data.login.error;
        this.isLoading = loading;
        return;
      }

      this.isLoading = loading;
      this.authService.setUserId(data.login.id);
      this.authService.navigateToProjects();
    });
  }

}
