import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { IUser } from '@modules/home/models/User';
import { AuthService } from '@modules/home/services/auth/auth.service';

@Component({
  selector: 'hours-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  registerForm: FormGroup = new FormGroup({});
  isLoading: boolean = true;
  error!: string;

  constructor(private authService: AuthService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(): void {
    this.registerForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      fullname: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required]),
    }, {
      validator: this.MustMatch('password', 'confirmPassword')
    });
  }

  register(): void {
    this.authService.register(this.formatUserData()).subscribe(({ data }) => {
      if (data?.registerUser.error !== null) {
        this.error = data?.registerUser.error;
        this.isLoading = false;
        return;
      }
      this.isLoading = true;
      this.authService.setUserId(data?.registerUser.id);
      this.authService.navigateToProjects();
    });;
  }

  private MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  private formatUserData(): IUser {
    const user = this.registerForm.getRawValue();
    return { fullname: user.fullname, email: user.email, password: user.password };
  }



}
