export interface IUser {
    fullname: string;
    email: string;
    password: string;
    confirmPassword?: string;
}

export interface IAuth {
    email: string;
    password: string;
}

export interface IResultAuth {
    id: number;
    isLoggedIn: boolean;
    error?: any;
}

// GraphQL Types
export interface TAuthLogin {
    login: IResultAuth;
}

export interface TAuthRegister {
    registerUser: IResultAuth;
}


