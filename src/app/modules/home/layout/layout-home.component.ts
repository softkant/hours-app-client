import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hours-layout-home',
  templateUrl: './layout-home.component.html',
  styleUrls: ['./layout-home.component.scss']
})
export class LayoutHomeComponent implements OnInit {
  isCollapsed:boolean = false;

  toggleCollapsed(): void {
    this.isCollapsed = !this.isCollapsed;
  }
  
  constructor() { }

  ngOnInit(): void {
  }

}
