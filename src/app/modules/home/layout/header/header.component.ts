import { Component, OnInit } from '@angular/core';
import { AuthService } from '@modules/home/services/auth/auth.service';

@Component({
  selector: 'hours-header-home',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  urlItemsNav!: any[];

  constructor(private authService:AuthService) { }

  ngOnInit(): void {
    this.setNavItems();
  }

  setNavItems(): void {
    this.urlItemsNav = [{ title: 'Home', url: "/" }, { title: 'About Us', url: "/about" }, { title: 'Price', url: "/price" }]
  }

  IsAutheticated(): boolean{
    return this.authService.isAuthenticated();
  }

  logout(): void{
    this.authService.logout();
  }


}
