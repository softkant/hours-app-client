import gql from 'graphql-tag';

export const REGISTER_USER = gql`
mutation($user:UserInputType!){
  registerUser(input:$user){
    id,
    isLoggedIn,
    error
  }
}
`;
