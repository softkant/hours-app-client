import gql from 'graphql-tag';

export const LOGIN_QUERY = gql`
query($user:LoginInputType!){
    login(input:$user){
      id,
      isLoggedIn,
      error
    }
  }
`;