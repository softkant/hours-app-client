import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from '@modules/project/pages/index/index.component';
import { GraphicComponent } from './pages/graphic/graphic.component';
import { ProjectComponent } from './pages/project/project.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { TeamComponent } from './pages/team/team.component';
import { TrackingComponent } from './pages/tracking/tracking.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: ':title',
    component: ProjectComponent,
    children: [
      { path: 'team', component: TeamComponent },
      { path: 'tracking', component: TrackingComponent },
      { path: 'reports', component: ReportsComponent },
      { path: '', redirectTo:"tracking" },
      { path: 'graphic', component: GraphicComponent },
    ],
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
