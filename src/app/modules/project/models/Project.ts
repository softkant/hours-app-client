export interface IProject {
    id?: number;
    title: string;
    description: string;
}

export interface IProjectId {
    id: number;
}

// GraphQL Types
export interface TProject  {
    projectUsers: IProject[];
  }

  export interface TProjectById  {
    projectByTitle: IProjectId;
  }


