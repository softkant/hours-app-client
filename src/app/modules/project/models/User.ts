

export interface IUserResultByProject {
    id:string;
    fullName: string;
    email: string;

}

// GraphQL Types
export interface TUserByProject  {
    projectUsersByIdProject: IUserResultByProject[];
  }

