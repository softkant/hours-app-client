export interface ITracking {
    id?: number;
    description: string;
    startTime: number;
    endTime: number;
    userid: number;
    projectid: number;
}

export interface ITracking_Filter {
    startTime: number;
    endTime: number;
    userId?: number ;
    projectid?: number;
}

export interface ITrackingFilter {
  startTime?: number;
  endTime?: number;
  userid?: number ;
  projectid?: number;
}

// GraphQL Types
export interface TTracking {
    createTracking: ITracking[];
}

export interface TTrackingFilter {
    filterTrackingByDate: ITracking[];
}



