import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IProject } from '@modules/project/models/Project';
import { ProjectService } from '@modules/project/services/project.service';

@Component({
  selector: 'hours-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  isNewProject: boolean = false;
  projects!: IProject[];
  projectForm!: FormGroup;
  isLoading: boolean = true;

  constructor(private projectService: ProjectService) { }

  ngOnInit(): void {
    this.getProjects();
    this.initializeForm();
  }

  newProject(): void {
    this.projectService.newProject(this.projectForm.getRawValue()).subscribe(data => {
      this.getProjects();
      this.projectForm.reset();
    });
  }

  getProjects(): void {
    this.projectService.getProjects().valueChanges.subscribe(({ data, loading }) => {
      this.isLoading = loading;
      this.projects = data.projectUsers;
    })
  }

  deleteProject(id: number): void {
    this.projectService.deleteProject(id).subscribe(data => this.getProjects());
  }

  private initializeForm(): void {
    this.projectForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });
  }

  isNewProjectChange(): boolean {
    return this.isNewProject = !this.isNewProject;
  }

}
