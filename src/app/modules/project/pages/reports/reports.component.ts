import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ITracking } from '@modules/project/models/Tracking';
import { IUserResultByProject } from '@modules/project/models/User';
import { ProjectService } from '@modules/project/services/project.service';
import { TrackingService } from '@modules/project/services/tracking.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';

@Component({
  selector: 'hours-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  projectId!: number;
  urlTitle!: string;
  users!: Observable<IUserResultByProject[]>;
  datesRangueForm!: FormGroup;
  tracking!: ITracking[];
  trackingForm!: FormGroup;
  isLoading: boolean = false;

  constructor(private projectService: ProjectService, private router: Router, private trackingService: TrackingService, private exportAsService: ExportAsService) { }

  ngOnInit(): void {
    this.getTitleByURL();
    this.getIdProject();
    this.initializeForm();
  }

  export(typeF: string): void {

    const exportAsConfig: any = {
      type: typeF,
      elementIdOrContent: 'tblTrack',
    }

    this.exportAsService.save(exportAsConfig, "Report " + this.urlTitle);
  }

  getUsers(): void {
    this.users = this.projectService.getUsersByProjectId(this.projectId).valueChanges.pipe(map(result => result.data.projectUsersByIdProject));
  }

  private getIdProject(): void {
    this.projectId = history.state.id;

    if (this.projectId == undefined) {
      this.projectService.getProjectIdByTitle(this.urlTitle).valueChanges.subscribe(data => {
        this.projectId = data.data.projectByTitle.id;
        this.getUsers();
        return;
      });
      return;
    }
    this.getUsers();
  }

  getTracking(userid:any=undefined): void {
    const datesRange = this.datesRangueForm.getRawValue();

    this.trackingService.filterTracking({ startTime: datesRange.startDate, endTime: datesRange.endDate, projectid: Number(this.projectId), userid: userid }).valueChanges.subscribe(({ data, loading }) => {
      this.isLoading = loading;
      this.tracking = data.filterTrackingByDate;
      this.datesRangueForm.reset();
    })
  }


  private getTitleByURL(): void {
    this.urlTitle = this.router.url.split('/')[2];
  }

  private initializeForm(): void {
    this.trackingForm = new FormGroup({
      description: new FormControl('', Validators.required),
      startTime: new FormControl('', Validators.required),
      endTime: new FormControl('', Validators.required),
      userid: new FormControl(''),
      projectid: new FormControl(''),
    });

    this.datesRangueForm = new FormGroup({
      startDate: new FormControl('', Validators.required),
      endDate: new FormControl('', Validators.required)
    });
  }


  getHours(startTime: number, endTime: number): any {
    var Difference_In_Time = endTime - startTime;
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    var Difference_In_Hours = 24 * Difference_In_Days;
    return this.transform(Difference_In_Hours * 60);

  }

  private transform(minutes: any) {
    const hours = Math.floor(minutes / 60);
    const minutesLeft = minutes % 60;
    return `${hours < 10 ? '0' : ''}${hours}:${minutesLeft < 10 ? '0' : ''}${minutesLeft}`
  }



}
