import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IUserResultByProject } from '@modules/project/models/User';
import { ProjectService } from '@modules/project/services/project.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'hours-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  users!: IUserResultByProject[];
  emailFormControl!: FormControl;
  projectId!: number;
  urlTitle!: string;
  isLoading: boolean = true;

  constructor(private projectService: ProjectService, private router: Router) { }

  ngOnInit(): void {
    this.getTitleByURL();
    this.getIdProject();
    this.initializedForm();
  }

  getUsers() {
    this.projectService.getUsersByProjectId(this.projectId).valueChanges.subscribe(({ data, loading }) => {
      this.isLoading = loading;
      this.users = data.projectUsersByIdProject;
    })
  }

  newCollaborator() {
    this.projectService.addNewUser(this.projectId, this.emailFormControl.value).subscribe(data => {
      this.emailFormControl.reset();
      this.getUsers();
    })
  }

  initializedForm() {
    this.emailFormControl = new FormControl('',[Validators.required, Validators.email]);
  }

  private getIdProject() {
    this.projectId = history.state.id;


    if (this.projectId == undefined) {
      this.projectService.getProjectIdByTitle(this.urlTitle).valueChanges.subscribe(data => {
        this.projectId = data.data.projectByTitle.id;
        this.getUsers();
        return;
      })
      return;
    }
    this.getUsers();
  }

  private getTitleByURL() {
    this.urlTitle = this.router.url.split('/')[2];
  }


}
