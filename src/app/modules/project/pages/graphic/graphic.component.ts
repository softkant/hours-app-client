import { Component, OnInit } from '@angular/core';
import { ProjectService } from '@modules/project/services/project.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ITracking_Filter } from '@modules/project/models/Tracking';

import {
  ChartComponent,
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};

@Component({
  selector: 'hours-graphic',
  templateUrl: './graphic.component.html',
  styleUrls: ['./graphic.component.scss']
})
export class GraphicComponent implements OnInit{
  public chartOptions: any;
  trackings!: ITracking_Filter[];
  public usertracking: any;
  public chartSeries: any;
  public chartLabels: any;
  //public trackings: any;
  projectId!: number;
  urlTitle!: string;

  constructor(private projectService:ProjectService, private router: Router) {
    this.usertracking = [];
    this.chartLabels = [];
    this.chartSeries = [];
    this.chartOptions = {
      series: this.chartSeries,
      chart: {
        width: 380,
        type: "pie"
      },
      labels: this.chartLabels,
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };
  }

  createChartData(){
    this.trackings.forEach(track => {
      var idexistente = 0;
      this.usertracking.forEach(t => {
        if(t.userid == track.userId){
          idexistente = t.userid;
          //suma de horas
          var horasActual = t.hours.replace(":","");
          var horasSumar = this.getHours(track.startTime, track.endTime).replace(":","");
          var suma = (parseInt(horasSumar) + parseInt(horasActual)).toString();
          if(suma.length  == 3){
            suma = "0" + suma;
          }
          t.hours = suma;
        }
      });
      if(idexistente == 0){
        var usertrack = {
          userid: track.userId,
          label: track.userId,
          hours: this.getHours(track.startTime, track.endTime)
        };
        this.usertracking.push(usertrack);
      }
    });
    this.usertracking.forEach(t => {
       var horasTotal = 0;
       if(t.hours.length  == 3){
          horasTotal = parseFloat(this.addStr(t.hours, 1, "."));
        }else{
          horasTotal = parseFloat(this.addStr(t.hours, 2, "."));
        }
        t.hours = horasTotal;
        this.chartLabels.push(t.label);
        this.chartSeries.push(t.hours);
    });
  }

  addStr(str, index, stringToAdd){
    return str.substring(0, index) + stringToAdd + str.substring(index, str.length);
  }

  getHours(startTime: number, endTime: number): any {
    var Difference_In_Time = endTime - startTime;
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    var Difference_In_Hours = 24 * Difference_In_Days;
    return this.transform(Difference_In_Hours * 60);

  }

  private transform(minutes: any) {
    const hours = Math.floor(minutes / 60);
    const minutesLeft = minutes % 60;
    return `${hours < 10 ? '0' : ''}${hours}:${minutesLeft < 10 ? '0' : ''}${minutesLeft}`
  }

  ngOnInit(): void {
    this.getIdProject();
    this.getTitleByURL();
    this.getTrackings();
  }

  private getIdProject() {
    this.projectId = history.state.id;

    if (this.projectId == undefined) {
      this.projectService.getProjectIdByTitle(this.urlTitle).valueChanges.subscribe(data => {
        this.projectId = data.data.projectByTitle.id;
      })
    }
  }

  private getTitleByURL() {
    this.urlTitle = this.router.url.split('/')[2];
  }

  getTrackings() {
    this.projectService.getTrackings(this.projectId).valueChanges.subscribe(data => {
      this.trackings = data.data.filterTrackingByDate;
      this.createChartData();
    });

  }
}
