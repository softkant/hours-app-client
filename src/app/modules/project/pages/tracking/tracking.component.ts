import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ITracking } from '@modules/project/models/Tracking';
import { ProjectService } from '@modules/project/services/project.service';
import { TrackingService } from '@modules/project/services/tracking.service';

@Component({
  selector: 'hours-tracking',
  templateUrl: './tracking.component.html',
  styleUrls: ['./tracking.component.scss']
})
export class TrackingComponent implements OnInit {
  urlTitle!: string;
  trackingForm!: FormGroup;
  projectId: number | undefined;
  isLoading: boolean = true;
  tracking!: ITracking[];

  constructor(private router: Router, private trackingService: TrackingService, private projectService: ProjectService) { }

  ngOnInit(): void {
    this.getTitleByURL();
    this.initializeForm();
    this.getIdProject();
  }

  private getTitleByURL() {
    this.urlTitle = this.router.url.split('/')[2];
  }

  private initializeForm(): void {
    this.trackingForm = new FormGroup({
      description: new FormControl('', Validators.required),
      startTime: new FormControl('', Validators.required),
      endTime: new FormControl('', Validators.required),
      userid: new FormControl(''),
      projectid: new FormControl(''),
    });
  }

  private getIdProject() {
    this.projectId = history.state.id;

    if (this.projectId == undefined) {
      this.projectService.getProjectIdByTitle(this.urlTitle).valueChanges.subscribe(data => {
        this.projectId = data.data.projectByTitle.id;
        this.getTracking();
        return;
      });
      return;
    }
    this.getTracking();
  }

  getTracking() {
    this.trackingService.filterTracking({ projectid: Number(this.projectId) }).valueChanges.subscribe(({ data, loading }) => {
      this.isLoading = loading;
      this.tracking = data.filterTrackingByDate;
    })
  }

  newTrack() {
    this.trackingForm.patchValue({ projectid: this.projectId })
    this.trackingService.newTracking(this.trackingForm.getRawValue()).subscribe(data => {
      this.trackingForm.reset();
      this.getTracking();
    });
  }

  getHours(startTime: number, endTime: number): any {
    var Difference_In_Time = endTime - startTime;
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    var Difference_In_Hours = 24 * Difference_In_Days;
    return this.transform(Difference_In_Hours * 60);

  }

  private transform(minutes: any) {
    const hours = Math.floor(minutes / 60);
    const minutesLeft = minutes % 60;
    return `${hours < 10 ? '0' : ''}${hours}:${minutesLeft < 10 ? '0' : ''}${minutesLeft}`
  }




}
