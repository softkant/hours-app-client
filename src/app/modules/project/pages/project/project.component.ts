import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { ProjectService } from '@modules/project/services/project.service';

@Component({
  selector: 'hours-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  urlTitle!: string;

  constructor(private projectService: ProjectService, private router: Router) {
  }

  ngOnInit(): void {
    this.getTitleByURL();
    this.verifyProjectTitle();
  }

  verifyProjectTitle() {
    this.projectService.getProjectIdByTitle(this.urlTitle).valueChanges.subscribe(data => {
      if (data.data.projectByTitle == null) {
        this.redirectToProjects();
        return;
      }

    })
  }

  private getTitleByURL() {
    this.urlTitle = this.router.url.split('/')[2];
  }

  private redirectToProjects() {
    this.router.navigate(["/projects"])
  }

}
