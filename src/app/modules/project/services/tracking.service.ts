import { Injectable } from '@angular/core';
import { AuthService } from '@modules/home/services/auth/auth.service';
import { Apollo } from 'apollo-angular';
import { NEW_TRACKING } from '../graphql/projects/mutations';
import { TRACKING_QUERY_FILTER } from '../graphql/projects/queries';
import { ITracking, ITrackingFilter, TTracking, TTrackingFilter } from '../models/Tracking';

@Injectable({
  providedIn: 'root'
})
export class TrackingService {

  constructor(private apollo: Apollo, private authService: AuthService) { }

  newTracking(tracking: ITracking) {
    console.log(tracking);
    return this.apollo.mutate({
      mutation: NEW_TRACKING,
      variables: {
        tracking: {
          description: tracking.description,
          starttime: tracking.startTime,
          endtime: tracking.endTime,
          userid: Number(this.authService.getUserId()),
          projectid: tracking.projectid
        }
      }
    });
  }

  filterTracking(tracking: ITrackingFilter) {

    return this.apollo.watchQuery<TTrackingFilter>({
      query: TRACKING_QUERY_FILTER,
      fetchPolicy: 'network-only',
      variables: {
        idUser: Number(this.authService.getUserId()),
        idProject: tracking.projectid,
        startTime: tracking.startTime ? Date.parse(tracking.startTime!.toString()) : undefined,
        endTime: tracking.endTime ? Date.parse(tracking.endTime!.toString()) : undefined
      }
    });
  }

}
