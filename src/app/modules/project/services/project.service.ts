import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { DELETE_PROJECT, NEW_PROJECT, NEW_USER } from '../graphql/projects/mutations';
import { PROJECTS_QUERY, PROJECT_QUERY_BY_TITLE, USERS_QUERY_FILTER, TRACKINGS_QUERY_BY_PROJECT } from '../graphql/projects/queries';
import { IProject, TProject, TProjectById } from '../models/Project';
import { TTracking, TTrackingFilter } from '../models/Tracking';
import { TUserByProject } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private apollo: Apollo) { }

  newProject(project: IProject) {
    return this.apollo.mutate({
      mutation: NEW_PROJECT,
      variables: {
        project: project
      }
    });
  }

  getProjects() {
    return this.apollo.watchQuery<TProject>({
      query: PROJECTS_QUERY,
      fetchPolicy: 'network-only'
    })
  }

  deleteProject(idProject: number) {
    return this.apollo.mutate({
      mutation: DELETE_PROJECT,
      variables: {
        id: idProject
      }
    });
  }

  getProjectIdByTitle(title: string) {
    return this.apollo.watchQuery<TProjectById>({
      query: PROJECT_QUERY_BY_TITLE,
      variables: {
        title: title
      }
    });
  }

  getUsersByProjectId(idProject: number) {
    return this.apollo.watchQuery<TUserByProject>({
      query: USERS_QUERY_FILTER,
      fetchPolicy: 'network-only',
      variables: {
        idProject: idProject
      }
    })
  }

  addNewUser(idProject: number, email: string) {
    return this.apollo.mutate({
      mutation: NEW_USER,
      variables: {
        projectId: idProject,
        email: email
      }
    });
  }

  getTrackings(idProject: number) {
     return this.apollo.watchQuery<TTrackingFilter>({
      query: TRACKINGS_QUERY_BY_PROJECT,
      variables: {
        idProject: idProject
      }
    });
  }
}
