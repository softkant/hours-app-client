import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutProjectComponent } from './layout/layout-project.component';
import { IndexComponent } from './pages/index/index.component';
import { ProjectRoutingModule } from './project-routing.module';
import { HeaderComponent } from './layout/header/header.component';
import { ProjectComponent } from './pages/project/project.component';
import { SharedModule } from '@shared/shared.module';
import { TeamComponent } from './pages/team/team.component';
import { AsideComponent } from './components/aside/aside.component';
import { TrackingComponent } from './pages/tracking/tracking.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { ExportAsModule } from 'ngx-export-as';
import { GraphicComponent } from './pages/graphic/graphic.component';
import { NgApexchartsModule } from "ng-apexcharts";


@NgModule({
  declarations: [LayoutProjectComponent, IndexComponent, HeaderComponent, ProjectComponent, TeamComponent, AsideComponent, TrackingComponent, ReportsComponent, GraphicComponent],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    ExportAsModule,
    NgApexchartsModule,
    SharedModule
  ]
})
export class ProjectModule { }
