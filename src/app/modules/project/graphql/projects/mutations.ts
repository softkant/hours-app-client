import gql from 'graphql-tag';

export const DELETE_PROJECT = gql`
mutation($id:ID!){
  deleteProject(id:$id){
   id
   }
 }
`;


export const NEW_PROJECT = gql`
mutation($project:ProjectInputType!){
  createProject(input:$project){
    id
  }
}
`;

export const NEW_TRACKING = gql`
mutation($tracking:TrackingInputType!){
  createTracking(input:$tracking){
    id
  }
}
`;

export const NEW_USER = gql`
mutation($projectId:ID!, $email:String!){
  createProjectUserByEmail(projectid:$projectId, email:$email){
    projectId,
    userId
  }
}
`;


