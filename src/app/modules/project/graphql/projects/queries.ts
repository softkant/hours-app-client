import gql from 'graphql-tag';

export const PROJECTS_QUERY = gql`
query{
  projectUsers{
		title,
    description,
    id
  }
}
`;

export const TRACKING_QUERY_FILTER = gql`
query($idUser:ID,$idProject:ID!, $startTime:ID, $endTime:ID){
  filterTrackingByDate(idUser:$idUser, idProject:$idProject, startTime: $startTime, endTime: $endTime){
    id,
    description,
    startTime,
    endTime,
    projectId,
    userId
    }
  }
`;

export const USERS_QUERY_FILTER = gql`
query($idProject:ID!){
  projectUsersByIdProject(idproject:$idProject){
    id,
    fullName,
    email
  }
}
`;

export const PROJECT_QUERY_BY_TITLE = gql`
query($title:String!){
  projectByTitle(title:$title){
    id
  }
}
`;

export const TRACKINGS_QUERY_BY_PROJECT = gql`
query($idProject:ID!){
  filterTrackingByDate(idProject: $idProject){
    id,
    userId,
    startTime,
    endTime
  }
}
`;



