import { Component, OnInit } from '@angular/core';
import { AuthService } from '@modules/home/services/auth/auth.service';

@Component({
  selector: 'hours-header-project',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  urlItemsNav!: any[];

  constructor(private authService:AuthService) { }

  ngOnInit(): void {
    this.setNavItems();
  }

  setNavItems(): void {
    this.urlItemsNav = [{ title: 'Home', url: "/" }, {title:'Use Cases', url:''}, {title:'Tutorials', url:''}]
  }

  logout(): void{
    this.authService.logout();
  }

}
