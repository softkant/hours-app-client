import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@modules/home/guards/auth.guard';
import { LayoutHomeComponent } from '@modules/home/layout/layout-home.component';
import { LayoutProjectComponent } from '@modules/project/layout/layout-project.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutHomeComponent,
    children: [
      {
        path: '', loadChildren: () => import('@modules/home/home.module').then(m => m.HomeModule)
      },
    ],
  },
  {
    path: 'projects',
    component: LayoutProjectComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '', loadChildren: () => import('@modules/project/project.module').then(m => m.ProjectModule)
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
