import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './core.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [CoreComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    CoreRoutingModule
  ],
  providers:[]
})
export class CoreModule {
}
